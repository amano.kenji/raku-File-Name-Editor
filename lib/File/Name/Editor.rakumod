# vim: filetype=raku
use File::Temp;
unit module File::Name::Editor;

sub edit-file-names(@input_files, Str $editor, Bool :$overwrite) is export {
  die "No input file passed." unless @input_files;
  die "File $_ doesn't exist" unless .IO.e for @input_files;
  is-unique(@input_files, "input");
  my @output_files = rename-files-in-editor($editor, @input_files);
  unless (elems @input_files) == (elems @output_files) {
    die "The number of input files ({elems @input_files}) doesn't match " ~
    "the number of output files ({elems @output_files}).";
  }
  is-unique(@output_files, "output");
  my $createonly=not $overwrite;
  for get_rename_list(@input_files, @output_files, :$overwrite) {
    rename .key, .value, :$createonly;
  }
}

sub is-unique(@files, Str $kind) {
  my $set = SetHash.new;
  for @files {
    if $_ (elem) $set {
      die "The filename '$_' appears multiple times in the $kind list.";
    }
    $set.set($_);
  }
}

sub rename-files-in-editor($editor, @input_files) {
  my ($outf, $outfh) = tempfile; # tempfile from File::Temp
  $outfh.print: join "\n", @input_files;
  run $editor, $outf;
  $outfh.seek: 0, SeekFromBeginning;
  return split "\n", $outfh.slurp.trim, :skip-empty;
}

sub get_rename_list(@input_files, @output_files, Bool :$overwrite) {
  my $input_set=@input_files.Set;
  my @rename_list=Array.new;
  for @input_files Z @output_files -> ($input, $output) {
    unless $input eq $output {
      if $output.IO.d {
        die "Cannot overwrite an existing directory: $output";
      } elsif $output.IO ~~ :f | :l {
        if $output (elem) $input_set {
          @rename_list.push($input => $output);
        } elsif $overwrite {
          @rename_list.push($input => $output);
        } else {
          die "The output file '$output' already exists. " ~
          "Use --overwrite to overwrite it.";
        }
      } else {
        @rename_list.push($input => $output);
      }
    }
  }
  return get_cyclic_rename_list(@rename_list);
}

# Check for cycles. If an input file is going to be renamed to another input
# file that hasn't been renamed yet in the rename list, the input file is
# first renamed to a temporary file and then to the output file.
sub get_cyclic_rename_list(@rename_list) {
  my $rename_set=@rename_list.map(*.key).SetHash;
  my @new_rename_list=Array.new;
  my @additional_rename_list=Array.new;
  for @rename_list {
    if .value (elem) $rename_set {
      my $temp_file=get_random_filename .key;
      @new_rename_list.push(.key => $temp_file);
      @additional_rename_list.push($temp_file => .value);
    } else {
      @new_rename_list.push(.key => .value);
    }
    $rename_set.unset(.key);
  }
  return (|@new_rename_list, |@additional_rename_list);
}

sub get_random_filename(Str $base) {
  for ^10 {
    my $rand = 100000.rand.Int;
    my $file = "$base-$rand";
    return $file unless $file.IO.e;
  }
  die "Failed to generate a random filename.";
}
