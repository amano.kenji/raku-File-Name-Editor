# File::Name::Editor

Rename files in any text editor that you like.

Cyclic renames such as A to B, B to C, and C to A are possible.

## Programmatic Usage

```
use File::Name::Editor;

my @files = ("file1", "file2", ...);
my $editor = %*ENV<EDITOR>;
edit-file-names(@files, $editor);
```

If there are problems, exceptions are thrown. Catch exceptions if necessary.

```
edit-file-names(@files, $editor, :overwrite);
```

allows `File::Name::Editor` to overwrite files not in `@files`.

## Command Line Usage

```
file-name-editor
```

prints how to use it.

## Installation

You can install it with `zef`.

```
zef install File::Name::Editor
```

This installs file-name-editor in `~/.raku/bin` or some other place.

If your linux distribution has it, you can install it with your distribution's
package manager.
